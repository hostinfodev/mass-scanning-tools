# mass-scanning-tools

-ONLY meant for good, don't be a jerk!

Various tools I have created for mass-scanning engagements. 

**Mass-Scanning** as in: Locating specific services like botnet C2's etc. over an entire ASN range.

I will add some better documentation when I have the time.

__[Programatically Chaining Output]:__

[ASN] -> asn2cidr.py -> Zmap (passing the port/probe module you are seeking) -> [raw/telnet/http discovery tool] -> Manual Examination

